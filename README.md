# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You need to have node version 12.19.0 & npm on your OS
* Clone the repo with git `git clone https://bitbucket.org/osirisbest/nppes.git`
* Run go to directory, run `npm i` for install dependencies
* You can work with default postgresql database `localhost` or change settings to your database
* Run `npm run start:debug`
* You can go to `http://localhost:4000/nppes/import?number=1467456137` where 1467456137 is target number
* For list of numbers fetch POST with body `{"number":"1467456137,1467456138"}` url 
`http://localhost:4000/nppes/import`
* For mask import fetch a POST to `http://localhost:4000/nppes/importByMask` with body 
`{"mask": "city=baltimore"}` where in `mask=x` x is a mask
* Open API with current state awailable on `http://localhost:4000/api`

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
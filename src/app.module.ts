import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NppesController } from './nppes/nppes.controller';
import { NppesService } from './nppes/nppes.service';
import { NppesModule } from './nppes/nppes.module';
import { nppes } from './nppes/nppes'

@Module({
  imports: [
    TypeOrmModule.forRoot({
        type: 'postgres',
        
        host: 'localhost',
        port: 5432,
        username: 'nest',
        password: 'nest',
        database: 'nest',
        entities: [nppes],
        synchronize: true,
      }),
    NppesModule
  ],
  controllers: [AppController, NppesController],
  providers: [AppService, NppesService],
})
export class AppModule {}

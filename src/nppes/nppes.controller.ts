import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { ImportByMask } from './importByMask.dto'
import { NppesService } from './nppes.service'

@Controller('nppes')
export class NppesController {
    constructor(private readonly nppesService:NppesService){}
    @Get(`import`)
    async import(@Query('number') number: string)
    {
        return await this.nppesService.import(number)
    }

    @Post(`import`)
    async importBody(@Body(`number`) numbers: string){
        return await this.nppesService.importArray(numbers)
    }

    @Post(`importByMask`)
    async importByMask(@Body() body: ImportByMask){
        const {mask} = body
        return await this.nppesService.importByMask(mask)
    }
}

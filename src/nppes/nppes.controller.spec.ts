import { Test, TestingModule } from '@nestjs/testing';
import { NppesController } from './nppes.controller';

describe('NppesController', () => {
  let controller: NppesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NppesController],
    }).compile();

    controller = module.get<NppesController>(NppesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

import { ApiProperty } from '@nestjs/swagger';
export class ImportByMask {

    @ApiProperty()
    readonly mask: string
}
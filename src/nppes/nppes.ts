import { Column, Entity, PrimaryGeneratedColumn, BaseEntity, CreateDateColumn, UpdateDateColumn} from "typeorm";

@Entity()
export class nppes extends BaseEntity {
    @PrimaryGeneratedColumn({ type: "integer", name: "id" })
    id: number;

    @CreateDateColumn({nullable: true, type: 'timestamp'})
    created: Date | null;
    
    @UpdateDateColumn({nullable: true, type: 'timestamp'})
    updated: Date | null;

    @Column({comment: 'number', nullable: true})
    number: string;

    @Column('simple-json')
    data: JSON

}
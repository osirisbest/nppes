import { Injectable } from '@nestjs/common';
import fetch from 'node-fetch'
import { nppes } from './nppes'
const uri = `https://npiregistry.cms.hhs.gov/api/?version=2.1&`
//const uri =`https://npiregistry.cms.hhs.gov/api/demo?version=2.1&`

@Injectable()
export class NppesService {

    async importByMask (mask: string)
    {
        const targetUri = `${uri}${mask}`
        const respons = await fetch(targetUri)
        const data = await respons.json()
        for (const item of data.results){
            const dataToSave = new nppes()
            dataToSave.number = item.number
            dataToSave.data = item
            await dataToSave.save()
        }
        return {rsult: `task done, ${data.result_count}`}
    }

    async importArray (numbers: string)
    {
        const targets = numbers.split(',')
        for (const target of targets){
            await this.import(target)
        }
        return `task done ${targets.length} items`
    }

    async import (number: string)
    {
        const respons = await fetch(`${uri}number=${number}`)
        const data = await respons.json()
        const dataToSave = new nppes()
        dataToSave.number = number
        dataToSave.data = data
        return await dataToSave.save()
    }
}

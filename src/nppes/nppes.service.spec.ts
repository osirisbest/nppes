import { Test, TestingModule } from '@nestjs/testing';
import { NppesService } from './nppes.service';

describe('NppesService', () => {
  let service: NppesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NppesService],
    }).compile();

    service = module.get<NppesService>(NppesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
